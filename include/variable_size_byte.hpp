/*
	Variable size byte data library

	This library is licensed under zlib license:

	============================================================================

	zlib License

	(C) 2022-present cam900 and contributors

	This software is provided 'as-is', without any express or implied
	warranty.  In no event will the authors be held liable for any damages
	arising from the use of this software.

	Permission is granted to anyone to use this software for any purpose,
	including commercial applications, and to alter it and redistribute it
	freely, subject to the following restrictions:

	1. The origin of this software must not be misrepresented; you must not
	claim that you wrote the original software. If you use this software
	in a product, an acknowledgment in the product documentation would be
	appreciated but is not required.
	2. Altered source versions must be plainly marked as such, and must not be
	misrepresented as being the original software.
	3. This notice may not be removed or altered from any source distribution.

	============================================================================

	Usage:
	- include this file at C++ source
	- [de/en]code_variable_size_byte_[signed/unsigned] to de/encode data

	Format:

	- Unsigned:
	  - 1 byte: 0aaa aaaa
	    - result: a
	  - 2 byte: 1aaa aaaa 0bbb bbbb
	    - result: 128 + a + (b * 128)
	  - 3 byte: 1aaa aaaa 1bbb bbbb 0ccc cccc
	    - result: 16512 + a + (b * 128) + (c * 16384)
	    ...
	  - N byte: 1aaa aaaa 1bbb bbbb 1ccc cccc ... 0NNN NNNN

	- Signed:
	  - 1 byte: 0saa aaaa
	    - result: a
	  - 2 byte: 1saa aaaa 0bbb bbbb
	    - result: 64 + a + (b * 64)
	  - 3 byte: 1saa aaaa 1bbb bbbb 0ccc cccc
	    - result: 8256 + a + (b * 64) + (c * 8192)
	    ...
	  - N byte: 1saa aaaa 1bbb bbbb 1ccc cccc ... 0NNN NNNN
	  - s: Sign bit
	    - result = -(result + 1) if set

*/
#ifndef VARIABLE_SIZE_BYTE_HPP
#define VARIABLE_SIZE_BYTE_HPP

#include <cstddef>

/*
	decode_variable_size_byte_[signed/unsigned]
	- decode variable size byte data from buffer
	- src (Input): buffer that stored variable size byte data
	- output (Output): where decoded data stores into
	- returns the size
*/
template<typename T>
std::size_t decode_variable_size_byte_unsigned(const unsigned char *src, T *output)
{
	T in = *src;
	if (in & 0x80) // multi-byte flag?
	{
		std::size_t len = 1;
		T res = in & 0x7f;
		int adj = 7;
		while (in & 0x80) // continuous data?
		{
			len++;
			in = *(++src); // fetch next byte
			res += ((in & 0x7f) << adj) + (1 << adj); // add to result
			adj += 7;
		}
		*output = res;
		return len;
	}

	*output = in;
	return 1;
}

template<typename T>
std::size_t decode_variable_size_byte_signed(const unsigned char *src, T *output)
{
	T in = 0; // data from file
	std::size_t size = decode_variable_size_byte_unsigned(src, &in);
	unsigned char flag = in & 0x40;                         // sign bit
	// convert to signed
	in = (in & 0x3f) | ((in & ~0x7f) >> 1);
	if (flag)
	{
		*output = (-in) - 1;
	}
	else
	{
		*output = in;
	}
	return size;
}

/*
	encode_variable_size_byte_[signed/unsigned]
	- encode variable size byte data and store into buffer
	- dst (Output): buffer that where encoded data stores into
	- input (Input): data to encode
	- returns the size
*/
template<typename T>
std::size_t encode_variable_size_byte_unsigned(unsigned char *dst, T input)
{
	if (input < 0x80) // 0-7f : Single byte
	{
		*dst = input;
		return 1;
	}
	else // 80 or larger: Multi byte
	{
		T prv = 0x80;
		T tmp = 0x80;
		int adj = 7;
		int siz = 0;
		std::size_t ret = 0;
		while (input >= tmp)
		{
			prv = tmp;
			siz++;
			adj += 7;
			tmp += 1 << adj;
		}
		input -= prv;
		while (siz)
		{
			*(dst++) = 0x80 | (input & 0x7f);
			siz--;
			input >>= 7;
			ret++;
		}
		*dst = (input & 0x7f);
		ret++;
		return ret;
	}
}

template<typename T>
std::size_t encode_variable_size_byte_signed(unsigned char *dst, T input)
{
	unsigned char sign = 0;       // sign bit
	if (input < 0) // input is negative?
	{
		input = -(input + 1);
		sign = 0x40;
	}
	// convert to unsigned
	T in = (input & 0x3f) | ((input & ~0x3f) << 1) | sign;
	return encode_variable_size_byte_unsigned(dst, in);
}

#endif